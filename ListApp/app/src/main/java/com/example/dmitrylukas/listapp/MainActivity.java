package com.example.dmitrylukas.listapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private TextView selection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //android:entries="@array/cities" - dobalili elementy iz xml
        selection = (TextView)findViewById(R.id.selected);
        final ListView citiesList = (ListView)findViewById(R.id.citieslist);

        //get cities from xml
        final String[]cities = getResources().getStringArray(R.array.cities);

        ArrayAdapter<String> adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, cities);
        citiesList.setAdapter(adapter);

        //add listener
        citiesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //for one selection:
                //String selectedItem = cities[position];


                //for multiple selection
                SparseBooleanArray sp = citiesList.getCheckedItemPositions();

                String selectedItems = "";
                for (int i = 0; i < cities.length; i++) {
                    if(sp.get(i))
                        selectedItems += cities[i] +", ";
                }
                selection.setText("Selected "+ selectedItems);
            }
        });
    }
}
