package com.example.dmitrylukas.myapplication2.feature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get msg from intent
        Intent intent = getIntent();
        String msg = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        //create TextField
        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(msg);

        //set to activity
        setContentView(textView);
    }
}
