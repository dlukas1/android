package com.example.dmitrylukas.myapplication2.feature;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    public  final static  String EXTRA_MESSAGE = "EXTRA_MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //method for handling button click
    public void sendMessage(View view){
        //create INTENT to call new activity
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        //get recent activity text field
        EditText et = (EditText)findViewById(R.id.edit_message);
        //get text
        String message = et.getText().toString();

        intent.putExtra(EXTRA_MESSAGE, message);

        //start activity
        startActivity(intent);
    }
}
