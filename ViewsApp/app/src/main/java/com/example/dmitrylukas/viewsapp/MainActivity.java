package com.example.dmitrylukas.viewsapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int clicks = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    public void  onPlus(View view){
        TextView clicksText = findViewById(R.id.clicksText);
        clicks++;
        clicksText.setText(clicks + " Clicks");
    }
    public void  onMinus(View view){
        TextView clicksText = findViewById(R.id.clicksText);
        clicks--;
        clicksText.setText(clicks + " Clicks");
    }

    public void reset(View view){
        TextView clicksText = findViewById(R.id.clicksText);
        clicks=0;
        clicksText.setText(clicks + " Clicks");
    }

    public void sendMessage(View view){
        TextView textView = (TextView) findViewById(R.id.textView);
        EditText editText = (EditText) findViewById(R.id.editText);
        textView.setText("Welcome to android, " + editText.getText());
    }
}
