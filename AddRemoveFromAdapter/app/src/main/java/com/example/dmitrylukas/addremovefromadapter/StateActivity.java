package com.example.dmitrylukas.addremovefromadapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StateActivity extends AppCompatActivity {

    private List<State>states = new ArrayList();
    ListView countriesList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state);

        //initialization
        setInitialData();
        //get list view element
        countriesList = (ListView)findViewById(R.id.countriesList);
        StateAdapter stateAdapter = new StateAdapter(this, R.layout.list_item, states);
        countriesList.setAdapter(stateAdapter);

        AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //get chosen item
                State selectedState = (State)parent.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), "You choosed "+ selectedState.getName(),
                        Toast.LENGTH_SHORT).show();
            }
        };
        countriesList.setOnItemClickListener(itemListener);
    }

    private void setInitialData(){
        //states = Arrays.asList(getResources().getStringArray(R.array.countries));
        states.add(new State("Russia", "Moscow", R.drawable.russia));
        states.add(new State("USA", "Washington", R.drawable.usa));
        states.add(new State("Germany", "Berlin", R.drawable.germany));
        states.add(new State("China", "Beijing", R.drawable.china));
        states.add(new State("Japan", "Tokio", R.drawable.japan));
    }
}
