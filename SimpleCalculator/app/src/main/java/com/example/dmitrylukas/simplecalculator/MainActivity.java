package com.example.dmitrylukas.simplecalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView resultField;
    EditText numberField;   //number input field
    TextView operationField;    //op. input field
    Double operand = null;  //operand
    String lastOperation = "="; //last operation

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //get all fields from activity_main.xml
        resultField = (TextView) findViewById(R.id.resultField);
        numberField = (EditText) findViewById(R.id.numberField);
        operationField = (TextView) findViewById(R.id.operationField);
    }

    //Save state
    @Override
    protected void onSaveInstanceState(Bundle outState){
        outState.putString("OPERATION", lastOperation);
        if (operand != null)
            outState.putDouble("OPERAND", operand);
        super.onSaveInstanceState(outState);
    }

    //get previously saved state
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        lastOperation = savedInstanceState.getString("OPERATION");
        operand = savedInstanceState.getDouble("OPERAND");
        resultField.setText(operand.toString());
        operationField.setText(lastOperation);
    }

    //handle number button click
    public void onNumberClick(View view){
        Button button = (Button)view;
        numberField.append(button.getText());

        if (lastOperation.equals("=") && operand != null){
            operand = null;
        }
    }

    //handle operation button press
    public void onOperationClick(View view){
        Button button = (Button) view;
        String op = button.getText().toString();
        String number = numberField.getText().toString();
        //if something is entered
        if(number.length()>0){
            number.replace(',', '.');
            try{
                performOperation(Double.valueOf(number), op);
            }   catch (NumberFormatException ex){
                numberField.setText("");
            }
        }
        lastOperation = op;
        operationField.setText(lastOperation);
    }

    private void performOperation(Double number, String operation){
        //first entry, no operand provided yet
        if (operand == null){
            operand = number;
        }   else {
            if (lastOperation.equals("=")){
                lastOperation = operation;
            }
            switch (lastOperation){

                case "=":
                    operand = number;
                    break;

                case "/":
                    if (number == 0){
                        operand = 0.0;
                    } else {
                        operand /= number;
                    }
                    break;

                case "*":
                    operand *= number;
                    break;

                case "-":
                    operand -= number;
                    break;

                    /*
                case "C":
                    resultField.setText("");
                    numberField.setText("");
                    operationField.setText("");
                    operand = null;
                    lastOperation = null;
                    break;

                case "SQRT":
                    operand = Math.sqrt(number);
                    break;

                case "Pow":
                    operand= Math.pow(number, 2);
                    break;
                    */

            }
        }
        resultField.setText(operand.toString().replace('.', ','));
        numberField.setText("");
    }

    public void showInfo(View view){
        Toast toast = Toast.makeText(this, "Dmitry Lukas (c) 2018", Toast.LENGTH_LONG);
        toast.show();
    }



}
