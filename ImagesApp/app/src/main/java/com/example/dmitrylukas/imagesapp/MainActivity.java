package com.example.dmitrylukas.imagesapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //This imageView will win
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.us2);


        setContentView(imageView);  //to bind from current code
        //setContentView(R.layout.activity_main); //to bind from XML

    }
}
