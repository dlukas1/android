package com.example.dmitrylukas.dataexchange;
import java.io.Serializable;

//public class Product implements Serializable{

public class Product implements Serializable{

    private String name;
    private String company;
    private int price;

    public Product(String n, String c, int p){
        name = n;
        company = c;
        price = p;
    }

    public String getName() {
        return name;
    }

    public String getCompany() {
        return company;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int p){
        price = p;
    }
}
