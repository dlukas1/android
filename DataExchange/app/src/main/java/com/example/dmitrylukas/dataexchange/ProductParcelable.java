package com.example.dmitrylukas.dataexchange;

import android.os.Parcelable;
import android.os.Parcel;

public class ProductParcelable implements Parcelable{

    private String name;
    private String company;
    private int price;

    public static final Creator<ProductParcelable> CREATOR = new Creator<ProductParcelable>() {
        @Override
        public ProductParcelable createFromParcel(Parcel source) {
            String name = source.readString();
            String comp = source.readString();
            int price = source.readInt();
            return new ProductParcelable(name, comp, price);
        }

        @Override
        public ProductParcelable[] newArray(int size) {
            return new ProductParcelable[size];
        }
    };

    public ProductParcelable(String n, String c, int p){
        name = n;
        company = c;
        price = p;
    }

    public String getName() {
        return name;
    }

    public String getCompany() {
        return company;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int p){
        price = p;
    }
    public void setCompany(String c){
        company = c;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags){
        parcel.writeString(name);
        parcel.writeString(company);
        parcel.writeInt(price);
    }
}
