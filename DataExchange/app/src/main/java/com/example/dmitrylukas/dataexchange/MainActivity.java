package com.example.dmitrylukas.dataexchange;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendData(View view){
        EditText nameText = findViewById(R.id.name);
        EditText compName = findViewById(R.id.company);
        EditText pricetext = findViewById(R.id.price);

        String name = nameText.getText().toString();
        String comp = compName.getText().toString();
        int price = Integer.parseInt(pricetext.getText().toString());

        Intent intent = new Intent(this, SecondActivity.class);
        Product product = new Product(name, comp, price);

        intent.putExtra(Product.class.getSimpleName(), product);
        //intent.putExtra("name", name);
        //intent.putExtra("comp", comp);
        //intent.putExtra("price", price);

        startActivity(intent);
    }
}
