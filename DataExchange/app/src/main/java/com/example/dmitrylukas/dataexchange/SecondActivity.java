package com.example.dmitrylukas.dataexchange;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView textView = new TextView(this);
        textView.setTextSize(20);
        textView.setPadding(20,20,20,20);

        Bundle extras = getIntent().getExtras();
        Product product;

        if(extras != null){

            //product = (Product) extras.getSerializable(Product.class.getSimpleName());

            /*
            String name = extras.get("name").toString();
            String comp = extras.get("comp").toString();
            int price = extras.getInt("price");
            */

            product = extras.getParcelable(ProductParcelable.class.getSimpleName());
            textView.setText("Name: " + product.getName() + "\nCompany: " + product.getCompany() + "\nPrice: " + product.getPrice());
        }

        setContentView(textView);
    }

    public void goBack(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
