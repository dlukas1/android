package com.example.dmitrylukas.controllitems;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onCheckBoxClicked(View view){
        //get flags
        CheckBox lang = (CheckBox)view;
        boolean isChecked = lang.isChecked();

        TextView selection = (TextView) findViewById(R.id.selection);
        if(lang.isChecked())
            selection.setText(lang.getText());
    }
}
