package com.example.dmitrylukas.listappwithbuttons;

public class Product {

    private String mName;
    private int mCount;
    private String mUnit;

    public Product(String name, String unit){
        mName = name;
        mUnit = unit;
        mCount = 0;
    }

    public String getName() {
        return mName;
    }

    public String getUnit() {
        return mUnit;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int mCount) {
        this.mCount = mCount;
    }

    public void setName(String mName) {
        this.mName = mName;
    }
}
