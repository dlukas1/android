package com.example.dmitrylukas.listappwithbuttons;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<Product> products = new ArrayList();
    ListView productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(products.size()==0){
            products.add(new Product("Sugar", "kg"));
            products.add(new Product("Chicken", "kg"));
            products.add(new Product("Eggs", "pcs"));
            products.add(new Product("Milk", "l"));
        }
        productList = (ListView)findViewById(R.id.productList);
        ProductAdapter adapter = new ProductAdapter(this, R.layout.item_list, products);
        productList.setAdapter(adapter);
    }
}
