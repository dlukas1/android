package com.example.dmitrylukas.activityforresultapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    static final String AGE_KEY = "AGE";
    static final String ACCSESS_MESSAGE = "ACCSESS_MESSAGE";
    private static final int REQUEST_ACCSESS_TYPE = 1;

    TextView textView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView)findViewById(R.id.textView);
    }

    public void onClick(View view){
        //get age provided
        EditText ageBox = (EditText)findViewById(R.id.ageBox);
        String age = ageBox.getText().toString();

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(AGE_KEY, age);
        startActivityForResult(intent, REQUEST_ACCSESS_TYPE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == REQUEST_ACCSESS_TYPE){
            if (resultCode == RESULT_OK){
                String accsessMessage = data.getStringExtra(ACCSESS_MESSAGE);
                textView.setText(accsessMessage);
            }
            else {
                textView.setText("Accsess failure");
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
