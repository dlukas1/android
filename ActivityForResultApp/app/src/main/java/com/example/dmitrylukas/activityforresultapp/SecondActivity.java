package com.example.dmitrylukas.activityforresultapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            TextView ageView = (TextView) findViewById(R.id.ageView);
            String age = extras.getString(MainActivity.AGE_KEY);
            ageView.setText("Age is " + age);
        }
    }

    public void onConfirmClick(View view){
        sendMessage("Accsess granted!");
    }

    public void onRejectClick(View view){
        sendMessage("Accsess rejected!");
    }

    public void onInvalidClick(View view){
        sendMessage("Invalid age!");
    }

    public void onCancelClick(View view){
        setResult(RESULT_CANCELED);
        finish();
    }

    private void sendMessage(String msg){
        Intent data = new Intent();
        data.putExtra(MainActivity.ACCSESS_MESSAGE, msg);
        setResult(RESULT_OK, data);
        finish();
    }
}
