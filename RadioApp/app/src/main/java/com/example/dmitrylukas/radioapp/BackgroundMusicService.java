package com.example.dmitrylukas.radioapp;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;

public class BackgroundMusicService extends Service implements
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnInfoListener{

    public static final String TAG = MainActivity.class.getSimpleName();
    private LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getBaseContext());

    private MediaPlayer mediaPlayer = new MediaPlayer();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        mediaPlayer.setOnInfoListener(this);
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        startPlayer();
        return START_REDELIVER_INTENT;
    }

    void startPlayer(){
        try {
            mediaPlayer.setDataSource("http://skymedia.babahhcdn.com/rrap");
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.prepareAsync(); //will continue in onPrepared

        Intent intent = new Intent(Constants.PlayerServiceStatusBuffering);

        manager.sendBroadcast(intent);
    }

    void stopPlayer(){
        mediaPlayer.stop();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroyCommand");
        stopPlayer();
        mediaPlayer = null;
        super.onDestroy();

    }

    //next method needs to comply superclass
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return null;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        Log.d(TAG, "onBuffering");
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "onCompletion");
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "onError");
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(TAG, "onPrepared");
        mediaPlayer.start();

    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "onInfo");
        return false;
    }
}
