package com.example.dmitrylukas.radioapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //for logging
    public static final String TAG = MainActivity.class.getSimpleName();

    private LocalBroadcastManager manager;

    private int PlayerStatus = Constants.PlayerStatusStopped;
    private Button ButtonMusicControl;
    private TextView TextViewArtist;
    private TextView TextViewTitle;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButtonMusicControl = (Button)findViewById(R.id.buttonMusicControl);
        TextViewArtist = (TextView)findViewById(R.id.artistText);
        TextViewTitle = (TextView)findViewById(R.id.titleText);

        updateControllButton();

        manager = LocalBroadcastManager.getInstance(getBaseContext());

        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.PlayerServiceStatusStopped);
        filter.addAction(Constants.PlayerServiceStatusPlaying);
        filter.addAction(Constants.PlayerServiceStatusBuffering);
        manager.registerReceiver(new LocalBroadcastReceiver(), filter);
    }


    public void onMusicCtrlButtonClicked(View view) {
        switch (PlayerStatus){
            case Constants.PlayerStatusStopped:
                PlayerStatus = Constants.PlayerStatusBuffering;
                BackgroundMusicServiceStart();
                break;
            case Constants.PlayerStatusBuffering:
                PlayerStatus = Constants.PlayerStatusStopped;
                BackgroundMusicServiceStop();
                break;
            case Constants.PlayerStatusPlaying:
                PlayerStatus = Constants.PlayerStatusStopped;
                BackgroundMusicServiceStop();
                break;
        }
        updateControllButton();
    }

    private void updateControllButton() {
        switch (PlayerStatus){
            case Constants.PlayerStatusStopped:
                ButtonMusicControl.setText(Constants.StatusTextStopped);
                break;
            case Constants.PlayerStatusBuffering:
                ButtonMusicControl.setText(Constants.StatusTextBuffering);
                break;
            case Constants.PlayerStatusPlaying:
                ButtonMusicControl.setText(Constants.StatusTextPlaying);
                break;
        }
    }

    private void BackgroundMusicServiceStart(){
        Log.d(TAG, "onBackgroundMusicServiceStart");
        Intent intent = new Intent(this, BackgroundMusicService.class);
        this.startService(intent);
    }

    private void BackgroundMusicServiceStop(){
        Log.d(TAG, "onBackgroundMusicServiceStart");
        Intent intent = new Intent(this, BackgroundMusicService.class);
        stopService(intent);
    }

    private class LocalBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "LocalBroadcastReceiver onReceive" + intent.getAction());
        }
    }
}
