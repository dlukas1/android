package com.example.dmitrylukas.radioapp;

public class Constants {

    public static final int PlayerStatusStopped = 0;
    public static final int PlayerStatusBuffering = 1;
    public static final int PlayerStatusPlaying = 2;

    public static final String StatusTextStopped = "Play";
    public static final String StatusTextBuffering = "Stop buffering";
    public static final String StatusTextPlaying = "Stop playing";

    public static final String PlayerServiceStatusBuffering = "com.example.dmitrylukas.player.service.status.buffering";
    public static final String PlayerServiceStatusPlaying = "com.example.dmitrylukas.player.service.status.playing";
    public static final String PlayerServiceStatusStopped = "com.example.dmitrylukas.player.service.status.stopped";


}
